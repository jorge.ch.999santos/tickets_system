import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import userRoutes from './routes/user.router';
import eventRoutes from './routes/event.router';
import bookingRoutes from './routes/booking.router';

import passportMiddleware from './middlewares/passport';
import passport from 'passport';
import passportLocal from 'passport-local';

import { dbConnection } from './database/config';

export class Server {
  app: any;
  port: string | undefined;
  paths: { 
    usuarios: string, 
    eventos: string,
    reservas: string, 
  };

  constructor() {
    this.app = express();
    this.port = process.env.NODE_LOCAL_PORT;
    this.paths = {
      usuarios: '/api/users',
      eventos: '/api/events',
      reservas: '/api/bookings'
    };

    //Conectar a Base de Datos
    this.conectarDB();

    //Middlewares
    this.middlewares();

    //Rutas de mi aplicación
    this.routes();
  }

  async conectarDB() {
    await dbConnection();
  }

  middlewares() {
    // CORS
    this.app.use(cors());
    this.app.use(morgan('dev'));
    // Lectura y parseo del body
    this.app.use(express.json());

    //Directorio Público
    this.app.use(express.static('public'));

    //Agregar para jwt
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(passport.initialize());
    passport.use(passportMiddleware);
  }

  routes() {
    this.app.use( this.paths.usuarios, userRoutes );
    this.app.use( this.paths.eventos, eventRoutes );
    this.app.use( this.paths.reservas, bookingRoutes );
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en puerto : ', this.port);
    });
  }
}
