import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity() // se puede pasar como parametro el nombre de tabla ej: 'usersTable'
export class Booking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  id_event: number;

  @Column()
  id_user: number;

  @Column()
  nameEvent: string;

  @Column({
    type: 'decimal',
    precision: 10,
    scale: 2,
    default: 0,
  })  
  price: number;

  @Column({default: '2023-12-01' })
  dateBooking: Date;

  @Column()
  place: string;

  @Column()
  gps: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
