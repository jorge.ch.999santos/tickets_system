import { DataSource } from "typeorm";
import { Booking, Event, User } from '../entities';

const DB_LOCAL_PORT = parseInt(process.env.MYSQL_LOCAL_PORT || '3306');

const AppDataSource = new DataSource({
    type: "mysql",
    host: process.env.MYSQL_HOST,
    port: DB_LOCAL_PORT,
    username: "root",
    password: process.env.MYSQL_ROOT_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    // logging: true, // muestra peticiones a la bd
    synchronize: true,
    entities: [User, Event, Booking],
});

export const dbConnection = async () => {
    try {
        await AppDataSource.initialize();
        console.log('Base de Datos Online =) ...');

    } catch ( error ) {
        console.log( error );
        throw new Error('Error al iniciar la Base de Datos');
    }
}

