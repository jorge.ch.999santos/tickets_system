import { Router } from "express";
import {
    getEvent,
    getEvents,
    postEvent,
    putEvent, 
    deleteEvent,
} from "../controllers/event.controller";
import passport from 'passport'

const router = Router();

router.get("/", getEvents);
router.get("/:id", getEvent);
router.post("/", passport.authenticate('jwt', { session: false} ), postEvent);
router.put("/:id", passport.authenticate('jwt', { session: false} ), putEvent);
router.delete("/:id", passport.authenticate('jwt', { session: false} ), deleteEvent);

export default router;