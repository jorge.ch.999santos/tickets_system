import { Router } from "express";
import {
    getUsers,
    getUser, 
    postUser, 
    putUser, 
    deleteUser
} from "../controllers/user.controller";
// import {getEvents} from "../controllers/event.controller";
import {signIn, signUp, protectedEndpoint, refresh } from '../controllers/user.controller';
import passport from 'passport'

const router = Router();

router.get("/", passport.authenticate('jwt', { session: false} ), getUsers);
router.get("/:id", passport.authenticate('jwt', { session: false} ), getUser);
router.post("/", passport.authenticate('jwt', { session: false} ), postUser);
router.put("/:id", passport.authenticate('jwt', { session: false} ), putUser);
router.delete("/:id", passport.authenticate('jwt', { session: false} ), deleteUser);


// Auth jwt
router.post('/signup', signUp);
router.post('/signin', signIn);
router.post('/token', refresh);
router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);

export default router;