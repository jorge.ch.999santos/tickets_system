import { Router } from "express";
import {
    getBookings,
    getBooking,
    postBooking,
    putBooking,
    deleteBooking,
} from "../controllers/booking.controller";
import passport from 'passport'

const router = Router();

router.get("/", passport.authenticate('jwt', { session: false} ), getBookings);
router.get("/:id", passport.authenticate('jwt', { session: false} ), getBooking);
router.post("/", passport.authenticate('jwt', { session: false} ), postBooking );
router.put("/:id", passport.authenticate('jwt', { session: false} ), putBooking);
router.delete("/:id", passport.authenticate('jwt', { session: false} ), deleteBooking);

export default router;