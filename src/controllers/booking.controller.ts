import { Request, Response } from "express";
import { Booking, Event, User } from "../entities";

import jwt, { JwtPayload } from "jsonwebtoken";

// Obtener reservas - Auth
export const getBookings = async (req: Request, res: Response) => {
  try {
    const bookings = await Booking.find();
    return res.json(bookings);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// Obtener reserva por id - Auth
export const getBooking = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const booking = await Booking.findOne({
      where: { id: parseInt(id) },
    });

    if (!booking) return res.status(404).json({ message: "Booking not found" });

    return res.json(booking);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

interface customRequest extends Request {
  token: string | JwtPayload;
}

// Crear reserva - Auth
export const postBooking = async (req: Request, res: Response) => {
  const token = req.header("Authorization")?.replace("Bearer ", "");
  // If token is not provided, send error message
  if (!token) {
    res.status(401).json({
      errors: [
        {
          msg: "Token not found",
        },
      ],
    });
  }

  try {
    const user = jwt.verify(
      token || "",
      process.env.PRIVATE_KEY_TOKEN || "somesecrettoken"
    );

    const { email } = <any>user;

    const userFound = <User>await User.findOneBy({ email: email });
    if (!userFound) {
      return res.status(400).json({ msg: "The User does not exists" });
    }

    if (!req.body.id_event) {
      return res.status(400).json({ msg: "Por favor envia el id del Evento" });
    }

    const eventFound = <Event>await Event.findOneBy({ id: req.body.id_event });
    if (!eventFound) {
      return res.status(400).json({ msg: "El evento no existe" });
    }

    const currentTickets: Booking[] = await Booking.findBy({
      id_event: eventFound.id,
    });

    if (eventFound.limit === 0 || currentTickets.length < eventFound.limit) {
      const newBooking = new Booking();
      newBooking.id_event = eventFound.id;
      newBooking.id_user = userFound.id;
      newBooking.nameEvent = eventFound.name;
      newBooking.price = eventFound.price;
      newBooking.dateBooking = eventFound.dateEvent;
      newBooking.place = eventFound.place;
      newBooking.gps = eventFound.gps;
      await newBooking.save();
      return res.status(201).json(newBooking);
    }
    return res.status(200).json({
      msg: "Lo sentimos no hay mas cupos =( .",
    });
  } catch (error) {
    res.status(403).json({
      errors: [
        {
          msg: "Invalid Token",
        },
      ],
    });
  }
};

// Actualizar reserva por id - Auth
export const putBooking = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const booking = await Booking.findOneBy({ id: parseInt(id) });
    if (!booking) return res.status(404).json({ message: "Not booking found" });

    await Booking.update({ id: parseInt(id) }, req.body);

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// Eliminar reserva por id - Auth
export const deleteBooking = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await Booking.delete({ id: parseInt(id) });

    if (result.affected === 0)
      return res.status(404).json({ message: "Booking not found" });

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};
