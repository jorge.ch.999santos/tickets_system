import { Request, Response } from "express";
import { Event } from "../entities";

// Obtener eventos - public
export const getEvents = async (req: Request, res: Response) => {
  try {
    const events = await Event.find();
    return res.json(events);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// Obtener evento por id - Auth
export const getEvent = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const event = await Event.findOne({
      where: { id: parseInt(id) },
    });

    if (!event) return res.status(404).json({ message: "Event not found" });

    return res.json(event);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// Crear evento - Auth
export const postEvent = async (req: Request, res: Response) => {
  if (
    !req.body.name ||
    !req.body.description ||
    !req.body.place ||
    !req.body.gps ||
    !req.body.price ||
    !req.body.limit ||
    !req.body.typeEvent
  ) {
    return res.status(400).json({ msg: "Por favor envia todos los campos" });
  }

  const event = await Event.findOneBy({ name: req.body.name });
  if (event) {
    return res.status(400).json({ msg: "El evento ya existe" });
  }

  const newEvent = new Event();
  newEvent.name = req.body.name;
  newEvent.description = req.body.description;
  newEvent.place = req.body.place;
  newEvent.dateEvent = req.body.dateEvent;
  newEvent.gps = req.body.gps;
  newEvent.price = req.body.price;
  newEvent.limit = req.body.limit;
  newEvent.typeEvent = req.body.typeEvent;
  await newEvent.save();
  return res.status(201).json(newEvent);
};

// Actualizar evento por id - Auth
export const putEvent = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const event = await Event.findOneBy({ id: parseInt(id) });
    if (!event) return res.status(404).json({ message: "Not event found" });

    await Event.update({ id: parseInt(id) }, req.body);

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// Eliminar evento por id - Auth
export const deleteEvent = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await Event.delete({ id: parseInt(id) });

    if (result.affected === 0)
      return res.status(404).json({ message: "Event not found" });

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};
