## **Sisteme backend de reserva de tickets**

Es una Aplicacion REST API que permite gestionar la reserva de tickets para distintos tipos de eventos:

- _CRUD de Usuarios con autenticación._
- _CRUD de Eventos con autenticación._
- _CRUD de Ticket (Reservas) con autenticación_
- _Autenticación con Jwt_
- _Base de Datos MYSQL con typeORM_
- _Si no esta logueado (SignIn) solo podra acceder a los eventos de forma pública._

### Entidades:

```ts
class User extends BaseEntity {
  id: number;
  email: string;
  password: string;
  active: boolean;
  createdAt: Date;
  updatedAt: Date;
}
```

```ts
class Event extends BaseEntity {
  id: number;
  name: string;
  description: string;
  place: string;
  dateEvent: Date;
  gps: string;
  price: number;
  limit: number;
  typeEvent: string;
  createdAt: Date;
  updatedAt: Date;
}
```

```ts
class Booking extends BaseEntity {
  id: number;
  id_event: number;
  id_user: number;
  nameEvent: string;
  price: number;
  dateBooking: Date;
  place: string;
  gps: string;
  createdAt: Date;
  updatedAt: Date;
}
```
Se agrego al modelo Booking el campo nameEvent para obtener una mayor claridad a la hora de realizar las pruebas a los endpoint con postman.

### **Archivo example.env**
Contiene todas las variables de entorno necesarias para que la aplicación trabaje correctamente. El archivo debe quedar de la siguiente manera ".env".

## **Archivo docker-compose.yml**
Contiene todas las configuraciones necesarias para poder trabajar con una base de datos MYSQL en un contenedor de forma local. Tambien requiere un archivo ".env" ya que tambien utiliza variables de entorno, con una observación la cual es que el archivo debe estar en el mismo directorio que el archivo "docker-compose.yml".
